package com.example.singer.okidoku;

import android.content.Context;

/**
 * Created by Singer on 9/9/2016.
 */
public class Util {
    public static float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }
}
